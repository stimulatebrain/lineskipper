"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'LineSkipper.menu.CustomMenu'
"""

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    """
    Custom Menu for LineSkipper admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.ModelList(
                '정보 관리',
                [
                 'apps.models.LsUser',
                 'apps.models.LsRestaurants',
                 'apps.models.LsPromoMaster',
                 'apps.models.LsRestaurantsReview',
                 'apps.models.LsOrderGroup',
                 'apps.models.LsOrder',
                 ]
            ),
            items.ModelList(
                 '마스터 테이블 관리',
                 ['apps.models.LsRestaurantsCategoryMaster',
                  'apps.models.LsRestaurantsMenuCategory',
                  'apps.models.LsRestaurantsReviewTagMaster',
                  ]
                ),
            items.MenuItem('통계',
                           children=[
                               items.MenuItem('접속 통계', '/foo/'),
                               items.MenuItem('GA 연결', '/bar/'),
                           ]
                           ),
            items.MenuItem('앱 관리',
                           children=[
                               items.MenuItem('피처드 리스트 설정', '/foo/'),
                               items.MenuItem('검색어 순위 설정', '/bar/'),
                               items.MenuItem('검색 반경 설정', '/bar/'),
                           ]
                           ),

        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
