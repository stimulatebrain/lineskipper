__author__ = 'jspark'

import os
from storages.backends.s3boto import S3BotoStorage

os.environ['S3_USE_SIGV4'] = 'True'


class MediaStorage(S3BotoStorage):
    location = 'media'

    @property
    def connection(self):
        if self._connection is None:
            self._connection = self.connection_class(
                self.access_key, self.secret_key,
                calling_format=self.calling_format, host='s3.ap-northeast-2.amazonaws.com')
        return self._connection

