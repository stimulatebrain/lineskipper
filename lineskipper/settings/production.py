__author__ = 'jspark'
from lineskipper.settings.base import *

DEBUG = False
ALLOWED_HOSTS = ["*"]


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'lineskipper',
        'USER': 'lineskipper',
        'PASSWORD': 'fprslwl1',
        'HOST': 'lineskipper.cju3crsk855u.ap-northeast-2.rds.amazonaws.com',
        'PORT': '3306',
        'OPTIONS': {'charset': 'utf8mb4'},
    },
}


CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
        "OPTIONS":
            {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            }
    }
}
