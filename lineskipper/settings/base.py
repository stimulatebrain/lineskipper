
import logging
from lineskipper import settings
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

SECRET_KEY = 'syys-95wu-^^cex2=*ygm+-emg0p7j_7=^p7l6&uai^vkj5ld*'

DEBUG = True

ALLOWED_HOSTS = ['*']

ADMINS = [('Jason', 'js.park@muzlive.com')]
SERVER_EMAIL = 'noreply@shoppin.it'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.worksmobile.com'
DEFAULT_FROM_EMAIL = "noreply@shoppin.it"

EMAIL_PORT = 587
EMAIL_HOST_USER = 'noreply@shoppin.it'
EMAIL_HOST_PASSWORD = 'vlrtm@23'

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'tabbed_admin',
    'nested_inline',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.messages',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.gis',

    # our apps
    'apps',
    # third party apps
    'storages',
    # 'debug_toolbar',
    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
    'mapwidgets',
    'rest_framework_swagger',
)

TABBED_ADMIN_USE_JQUERY_UI = True

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],

    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )

}

# STATIC_PRECOMPILER_COMPILERS = (
#     'static_precompiler.compilers.SCSS',
# )

SITE_ID = 1

# BROKER_URL = 'django://lineskipper'
# INSTALLED_APPS += ('kombu.transport.django', )

MIDDLEWARE = ['django.contrib.sessions.middleware.SessionMiddleware',
                      'django.contrib.auth.middleware.AuthenticationMiddleware',
                      'django.middleware.common.CommonMiddleware',
                      'django.middleware.csrf.CsrfViewMiddleware',
                      # 'debug_toolbar.middleware.DebugToolbarMiddleware',
                      'django.contrib.messages.middleware.MessageMiddleware']

INTERNAL_IPS = ('127.0.0.1', )

WSGI_APPLICATION = 'lineskipper.wsgi.application'


SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Seoul'


TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
        # insert your TEMPLATE_DIRS here
        os.path.join(BASE_DIR, "templates"),
    ],
    # 'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': [
            # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
            # list if you haven't customized them:
            'django.contrib.auth.context_processors.auth',
            'django.template.context_processors.debug',
            'django.template.context_processors.i18n',
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.tz',
            'django.contrib.messages.context_processors.messages',

            'django.template.context_processors.request',
        ],
        'loaders': [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
            'admin_tools.template_loaders.Loader',
        ],
        'debug': DEBUG,
    },
},
]
#ADMIN_TOOLS_MENU = 'yourproject.menu.CustomMenu'


LANGUAGE_CODE = 'ko-KR'

TIME_ZONE = 'Asia/Seoul'

USE_I18N = False

USE_L10N = False

USE_TZ = False

STATIC_URL = '/static/'
LOGIN_URL = 'rest_framework:login'
LOGOUT_URL = 'rest_framework:logout'


STATICFILES_DIRS = (os.path.join(BASE_DIR, "assets"),)


STATIC_ROOT = 'staticfiles'


STATICFILES_FINDERS = (
 'django.contrib.staticfiles.finders.FileSystemFinder',
 'django.contrib.staticfiles.finders.AppDirectoriesFinder',
 # other finders..
 #
    )

ADMIN_TOOLS_MENU = 'lineskipper.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'lineskipper.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'lineskipper.dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_THEMING_CSS = 'css/theming.css'

MEDIAFILES_LOCATION = 'media'

AWS_ACCESS_KEY_ID = 'AKIAIEAGOXHULKPDQ5EQ'
AWS_SECRET_ACCESS_KEY = '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'
DEFAULT_FILE_STORAGE = 'lineskipper.settings.storage.MediaStorage'
#DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'


AWS_QUERYSTRING_AUTH = False
AWS_STORAGE_BUCKET_NAME = 'lineskipper-image'
AWS_S3_HOST = 's3.ap-northeast-2.amazonaws.com'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
MEDIA_URL = 'https://%s/media/' % AWS_S3_CUSTOM_DOMAIN

AUTH_USER_MODEL = "apps.LsUser"
SESSION_COOKIE_AGE = 14515200

# LOGGING = {
# 	'version': 1,
# 	'disable_existing_loggers': False,
# 	'handlers': {
# 		'console': {
# 			'level': 'DEBUG',
# 			'class': 'logging.StreamHandler',
# 		}
# 	},
# 	'loggers': {
# 		'django.db.backends': {
# 			'level': 'DEBUG',
# 		},
# 	}
# }


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s [%(asctime)s] %(module)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': './logs/logs.log',
            'maxBytes': 1024000,
            'backupCount': 3,
        },
        'sql': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': './logs/sql.log',
            'maxBytes': 102400,
            'backupCount': 3,
        },
        'commands': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': './logs/commands.log',
            'maxBytes': 10240,
            'backupCount': 3,
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'console', 'mail_admins', ],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django.db.backends': {
            'handlers': ['sql', 'console'],
            'propagate': False,
            'level': 'DEBUG',
        },
        'scheduling': {
            'handlers': ['commands', 'console'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django.template': {
            'handlers': ['console'],  # Quiet by default!
            'propagate': False,
            'level': 'WARNING',
        },
    }
}

ROOT_URLCONF = 'apps.urls'

MAP_WIDGETS = {
    "GooglePointFieldWidget": (
        ("zoom", 15),
        ("mapCenterLocationName", "seoul"),
        ("GooglePlaceAutocompleteOptions", {'componentRestrictions': {'country': 'kr'}}),
        ("markerFitZoom", 12),
    ),
    "GoogleStaticMapWidget": (
        ("zoom", 15),
        ("size", "320x320"),
        ("thumbnail_size", "100x100"),
    ),
    "GoogleStaticMapMarkerSettings": (
        ("color", "green"),
    ),
    "GOOGLE_MAP_API_KEY": "AIzaSyCZUJcp0fsG_1EFOpCFviyi7XS7QN7pwLQ"
}


if DEBUG:
    # will output to your console
    logging.basicConfig(
        level = logging.DEBUG,
        format = '%(asctime)s %(levelname)s %(message)s',
    )
# logging.getLogger('boto').setLevel(logging.INFO)
# logging.getLogger('PIL').setLevel(logging.INFO)

else:
    logging.getLogger('boto').setLevel(logging.CRITICAL)
    logging.getLogger('django.db.backends').setLevel(logging.CRITICAL)