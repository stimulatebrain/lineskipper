from apps.admin_parts import TabbedModelAdminForDjango2, NestedInlineReadOnly, InlineReadOnly
from apps.models import *
from django.apps import apps
from django.conf import settings
from django.contrib import admin
from django.contrib.admin import AdminSite, ModelAdmin
from django.contrib.admin.widgets import AdminDateWidget
from django.utils.translation import ugettext_lazy
from mapwidgets.settings import mw_settings
from django import forms

# app = apps.get_app_config('apps')
# for model_name, model in app.models.items():
#     model_admin = type(model_name + "Admin", (admin.ModelAdmin,), {})
#
#     model_admin.list_display = model.admin_list_display if hasattr(model, 'admin_list_display') else tuple([field.name for field in model._meta.fields])
#     model_admin.list_filter = model.admin_list_filter if hasattr(model, 'admin_list_filter') else model_admin.list_display
#     model_admin.list_display_links = model.admin_list_display_links if hasattr(model, 'admin_list_display_links') else ()
#     model_admin.list_editable = model.admin_list_editable if hasattr(model, 'admin_list_editable') else ()
#     model_admin.search_fields = model.admin_search_fields if hasattr(model, 'admin_search_fields') else ()
#
#     admin.site.register(model, model_admin)


from django.contrib.gis.db import models
from mapwidgets.widgets import GooglePointFieldWidget, minify_if_not_debug, BasePointFieldMapWidget, \
    GoogleStaticOverlayMapWidget
from nested_inline.admin import NestedTabularInline, NestedModelAdmin, NestedStackedInline


class RestaurauntNestedInline(NestedInlineReadOnly):
    model = LsRestaurants
    exclude = ('update_dt', 'insert_dt', )


class UserFavoriteInline(InlineReadOnly):
    model = LsUserRestaurantsFavorite
    extra = 1
    exclude = ('update_dt', 'insert_dt', )


class OrderInline(NestedInlineReadOnly):
    model= LsOrder
    exclude=('update_dt', 'insert_dt', 'rest')

    def has_add_permission(self, request):
        return False


class OrderGroupInline(NestedInlineReadOnly):
    model = LsOrderGroup
    exclude = ('update_dt', 'promo')
    can_delete = False
    extra = 0
    inlines = [OrderInline]

    def has_add_permission(self, request):
        return False


class RestaurantHolidayInline(admin.TabularInline):
    model = LsRestaurantsHoliday
    exclude = ('insert_dt', 'update_dt')
    formfield_overrides = {
        models.DateField : {
            "widget" : AdminDateWidget
        }
    }


class MenuCategoryInline(admin.TabularInline):
    model = LsRestaurantsMenuCategory
    exclude = ('insert_dt', 'update_dt')


class MenuAddonInline(NestedStackedInline):
    model = LsRestaurantsMenuAddon
    exclude = ('insert_dt', 'update_dt')
    extra = 1


class MenuExtraInline(NestedStackedInline):
    model = LsRestaurantsMenuExtra
    exclude = ('insert_dt', 'update_dt')
    extra = 1


class MenuInline(NestedStackedInline):
    model = LsRestaurantsMenu
    # raw_id_fields = ("category", )
    inlines = (MenuAddonInline, MenuExtraInline, )
    exclude = ('insert_dt', 'update_dt')


class RestaurantCategoryInline(admin.StackedInline):
    model = LsRestaurantsCategory
    raw_id_fields = ("category",)
    exclude = ('insert_dt', 'update_dt')
    extra = 2


class RestaurantOpenHoursInline(admin.StackedInline):
    model = LsRestaurantsOperationHour
    exclude = ('insert_dt', 'update_dt')
    extra = 1
    max_num = 1
    min_num = 1


@admin.register(LsRestaurantsMenuCategory)
class RestaurantMenuCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    exclude = ('insert_dt', 'update_dt')


@admin.register(LsRestaurantsCategoryMaster)
class RestaurantCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'group_name', 'insert_dt')
    exclude = ('insert_dt', 'update_dt')


class RestaurantHeaderImageInline(admin.StackedInline):
    model = LsRestaurantsHeaderImages
    exclude = ('insert_dt', 'update_dt', )


@admin.register(LsRestaurants)
class RestaurantAdmin(TabbedModelAdminForDjango2, NestedModelAdmin):
    list_display = ('name', 'is_activate', 'tel', 'address', 'rest_no')
    readonly_fields = ('rest_no',)
    exclude = ('insert_dt', 'update_dt')
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

    tab_overview = (
        (None, {
            'fields': ('rest_no', 'name', 'is_activate', 'tel', 'address', 'desc', 'tax_percent', 'logo_url',
                       'service_fee_amount', 'service_fee_type', 'min_order_time',
                       'reserve_time_gap', 'reserve_max_count')
        }),
        RestaurantCategoryInline,
        ('Address', {
            'fields': ('address', 'geo_point')
        })
    )
    tab_menu = (
        MenuInline,
    )

    tab_open_hour = (
        RestaurantOpenHoursInline,
        RestaurantHolidayInline,
    )

    tab_header = (
        RestaurantHeaderImageInline,
    )

    tabs = [
        ('Overview', tab_overview),
        ('Menu', tab_menu),
        ('Open Hour', tab_open_hour),
        ('Header Images', tab_header),
    ]


@admin.register(LsUser)
class UserAdmin(TabbedModelAdminForDjango2, NestedModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone', 'type')
    exclude = ('insert_dt', 'update_dt')

    tab_overview = (
        ('Overview', {
            'fields': ('first_name', 'last_name',  'email', 'phone', 'type', 'groups', 'user_permissions', 'credit',
                       'thumbnail_url', 'use_mile', 'is_staff', 'is_superuser', 'is_active')
        }),
    )
    tab_orders = (

    )

    tab_favorites = (

    )

    tabs = [
        ('Overview', tab_overview),
        ('Orders', tab_orders),
        ('Favorites', tab_favorites),
    ]

    def get_tabs(self, request, obj=None):
        tabs = self.tabs
        if obj:
            if LsOrderGroup.objects.filter(user_id=obj.id).count() > 0:
                self.tab_orders = (OrderGroupInline, )
            if LsUserRestaurantsFavorite.objects.filter(user_id=obj.id).count() > 0:
                self.tab_favorites = (UserFavoriteInline, )

            tabs = [
                ('Overview', self.tab_overview),
                ('Orders', self.tab_orders),
                ('Favorites', self.tab_favorites),
            ]
        self.tabs = tabs
        return super(UserAdmin, self).get_tabs(request, obj)


@admin.register(LsPromoMaster)
class PromoAdmin(ModelAdmin):
    list_display = ('id', 'desc', 'code', 'amount', 'type', 'start_dt', 'end_dt')
    exclude = ('insert_dt', 'update_dt')


@admin.register(LsRestaurantsReviewTagMaster)
class ReviewTagMasterAdmin(ModelAdmin):
    list_display = ('id', 'name')
    exclude = ('insert_dt', 'update_dt')


class ReviewTagInline(InlineReadOnly):
    model = LsRestaurantsReviewTag
    extra = 1
    exclude = ('insert_dt', 'update_dt')


class ReviewPhotosInline(InlineReadOnly):
    model = LsRestaurantsReviewPhoto
    exclude = ('insert_dt', 'update_dt')


@admin.register(LsRestaurantsReview)
class ReviewAdmin(ModelAdmin):
    list_display = ('id', 'rest', 'user', 'rating', 'contents', )
    exclude = ('insert_dt', 'update_dt')
    inlines = (ReviewTagInline, ReviewPhotosInline)
    search_fields = ('rest__name', 'user__first_name', 'user__last_name', 'user__email',)


@admin.register(LsOrderGroup)
class OrderGroupAdmin(ModelAdmin):
    list_display = ('id', 'user', 'pay_method', 'pay_total_price', 'orig_price', 'pay_credit', )
    exclude = ('insert_dt', 'update_dt')
    inlines = (OrderInline, )
    search_fields = ('user__first_name', 'user__last_name', 'user__id')


class OrderAddonInline(NestedInlineReadOnly):
    model = LsOrderItemAddon
    extra = 1
    exclude = ('insert_dt', 'update_dt')


class OrderItemInline(NestedInlineReadOnly):
    model = LsOrderItem
    exclude = ('insert_dt', 'update_dt', 'is_recommended')
    inlines = (OrderAddonInline, )


@admin.register(LsOrder)
class OrderAdmin(NestedModelAdmin):
    list_display = ('id', 'order_no', 'rest', 'status_code', 'tip_price', 'reserve_time', 'pay_price', )
    exclude = ('insert_dt', 'update_dt', 'order_group')
    inlines = (OrderItemInline, )
    search_fields = ('rest__name', 'order_no')
