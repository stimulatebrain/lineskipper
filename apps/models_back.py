# -*- coding: utf-8 -*-

from datetime import datetime
import StringIO
from mimify import File
import os
import urllib
import urllib2
import boto
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from PIL import Image
from django.core.files.storage import default_storage as storage
import requests
from django.core.files.temp import NamedTemporaryFile


def image_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    filename = datetime.now().strftime("%Y%m%d_%H%M%S_%f") + os.path.splitext(filename)[1]
    return 'uploads/' + datetime.now().strftime("%Y/%m/%d/") + filename


class UserManager(BaseUserManager):
    def create_user_naver(self, email, password, name, naver_id, naver_token, gender, age_range):
        user = self.model(email=UserManager.normalize_email(email), name=name, gender=gender,
                          age_range=age_range, naver_token=naver_token, naver_id=naver_id)
        user.set_password(password)
        user.save(using=self._db)
        return user



    def create_user_instagram(self, email, password, name, insta_id, insta_token):
        user = self.model(email=UserManager.normalize_email(email), name=name, insta_id = insta_id, insta_token = insta_token)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user_facebook(self, email, password, name, fb_id, gender, age_range, fb_token):
        user = self.model(email=UserManager.normalize_email(email), name=name, fb_id=fb_id, gender=gender,
                          age_range=age_range, fb_token=fb_token)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, name):
        user = self.model(email=UserManager.normalize_email(email), name=name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, name):
        user = self.create_user(email, password=password, name=name)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        db_table = 'user'
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=32)
    imageUrl = models.CharField(max_length=1024, blank=True)
    insertDate = models.DateTimeField(auto_now_add=True)

    gender = models.CharField(max_length=10, null=True)
    age_range = models.CharField(max_length=128, null=True)

    naver_id = models.BigIntegerField(null=True)
    insta_id = models.BigIntegerField(null=True)
    fb_token = models.CharField(max_length=2048, null=True)
    naver_token = models.CharField(max_length=1024, null=True)
    insta_token = models.CharField(max_length=2048, null=True)
    fb_id = models.BigIntegerField(null=True)

    point = models.IntegerField(null=False, default=0)
    def_address = models.IntegerField(null=True)

    customer_name = models.CharField(max_length=45, null=True)
    customer_phone = models.CharField(max_length=45, null=True)
    customer_phone_verify_yn = models.CharField(max_length=1, null=False, default='N')
    customer_email = models.CharField(max_length=255, null=True)
    birthday = models.CharField(max_length=20, null=True)
    is_receive_mail_yn = models.CharField(max_length=1, default='N')
    last_visit = models.DateTimeField()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name", ]

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_vendor = models.BooleanField(default=False)

    auth_token = models.CharField(max_length=40, null=True)

    style_result = models.ForeignKey('StyleOlympicResult', db_constraint=False, null=True)

    objects = UserManager()

    def get_short_name(self):
        return self.email


class UserAndShopRelation(models.Model):
    class Meta:
        db_table = 'user_and_shop_relation'

    user = models.ForeignKey(User, db_constraint=False, null=True,
                             related_name='relation_shops')
    shop = models.ForeignKey('Shop', db_constraint=False, null=True,
                             related_name='relation_users')
    insertDate = models.DateTimeField(auto_now_add=True)


class Shop(models.Model):
    class Meta:
        db_table = 'shop'
    name = models.CharField(max_length=100)
    logoUrl = models.ImageField(upload_to=image_directory_path)
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')
    send_cost = models.IntegerField(null=True)
    send_company = models.CharField(max_length=255, null=True)
    send_homepage = models.CharField(max_length=1024, null=True)
    free_send_price = models.IntegerField(null=True)

    insta_name = models.CharField(null=True, max_length=255)
    insta_cover_url = models.ImageField(upload_to=image_directory_path, null=True)
    insta_profile_url = models.ImageField(upload_to=image_directory_path, null=True)
    insta_profile_desc = models.CharField(null=True, max_length=1024)

    insta_cover_link = models.CharField(null=True, max_length=1024)

    #SHOPPIN CART 정보
    cart_desc = models.CharField(null=True, max_length=50)
    cart_logoUrl = models.ImageField(upload_to=image_directory_path, null=True)
    cart_logo_file = models.CharField(max_length=255, null=True)
    cart_coverUrl = models.ImageField(upload_to=image_directory_path, null=True)
    cart_cover_file = models.CharField(max_length=255, null=True)

    registration_no = models.CharField(null=True, max_length=128)
    sell_registration_no = models.CharField(null=True, max_length=128)
    tel = models.CharField(null=True, max_length=128)
    email = models.CharField(null=True, max_length=128)

    #계좌 정보
    payment_method = models.ForeignKey('PaymentMethod', db_constraint=False, null=True)
    bank_name = models.CharField(null=True, max_length=128)
    bank_no = models.CharField(null=True, max_length=1024)
    bank_holder = models.CharField(null=True, max_length=128)

    # sms 정보
    acount_sms = models.CharField(null=True, max_length=1024)
    pay_completed_sms = models.CharField(null=True, max_length=1024)
    isAutoSendSMS = models.CharField(max_length=1, default='N')

class Editor(models.Model):
    class Meta:
        db_table = 'editor'
    name = models.CharField(max_length=100)


class Look(models.Model):
    class Meta:
        db_table = 'look'
    user = models.ForeignKey(User, db_constraint=False)
    shop = models.ForeignKey(Shop, db_constraint=False, null=True)
    editor = models.ForeignKey(Editor, db_constraint=False, null=True)

    isDelYN = models.CharField(max_length=1, default='N')
    viewCount = models.IntegerField(default=0)
    shareCount = models.IntegerField(default=0)
    insertDate = models.DateTimeField(auto_now_add=True)
    updateDate = models.DateTimeField()
    desc = models.CharField(max_length=1024, null=True)
    isOpenYN = models.CharField(max_length=1, default='Y')
    look_type = models.CharField(max_length=45, default='MAIN')
    insta_item_id = models.CharField(max_length=1024, null=True)
    insta_caption = models.CharField(max_length=2048, null=True)
    insta_use_yn = models.CharField(max_length=1, null=False, default='N')

    def duplicate(self):
        kwargs = {}
        for field in self._meta.fields:
            kwargs[field.name] = getattr(self, field.name)
            # or self.__dict__[field.name]
        kwargs.pop('id')
        new_instance = self.__class__(**kwargs)
        new_instance.save()
        # now you have id for the new instance so you can
        # create related models in similar fashion
        # foreign key로 look 참조하고있는 모델들. 가져오는 메소드가 없음.
        fkeys = [self.areas, self.contents, self.items, self.likes,]

        for fkey in fkeys:
            for item in fkey.all():
                kwargs = {}
                for field in item._meta.fields:
                    kwargs[field.name] = getattr(item, field.name)
                kwargs.pop('id')
                temp_new_instance = item.__class__(**kwargs)
                temp_new_instance.look_id = new_instance.id
                temp_new_instance.save()

        if hasattr(self, 'classification'):
            kwargs = {}
            for field in self.classification._meta.fields:
                kwargs[field.name] = getattr(self.classification, field.name)
            temp_new_instance = self.classification.__class__(**kwargs)
            temp_new_instance.look_id = new_instance.id
            temp_new_instance.id = None
            temp_new_instance.save()

        return new_instance


class LookContents(models.Model):
    class Meta:
        db_table = 'look_contents'
    look = models.ForeignKey(Look, db_constraint=False, related_name='contents')
    insertDate = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=10, default='IMAGE')
    url = models.ImageField(upload_to=image_directory_path)
    isDelYN = models.CharField(max_length=1, default='N')
    isFromWebYN = models.CharField(max_length=1, default='N')
    web_url = models.URLField(null=True)

    def save(self, *args, **kwargs):
        # Insta From Web 추가
        if self.isFromWebYN == 'Y':
            input_file = StringIO.StringIO(urllib2.urlopen(self.web_url).read())
            output_file = StringIO.StringIO()
            img = Image.open(input_file)
            if img.mode != "RGB":
                img = img.convert("RGB")
            img.save(output_file, "JPEG")
            self.url.save(
                datetime.now().strftime("%Y%m%d_%H%M%S_%f.jpg"),
                ContentFile(output_file.getvalue()),
                save=False)

            try:
                # create thumbnail
                file_path = self.url.name
                filename_base, filename_ext = os.path.splitext(file_path)
                thumb_file_path = "%s_thumb.jpg" % filename_base

                # resize the original image and return url path of the thumbnail
                img.thumbnail((self.url.width / 2, self.url.height / 2), Image.ANTIALIAS)
                f_thumb = storage.open(thumb_file_path, "w+")

                # url 로 업로드할때 f_thumb으로 바로 업로드하면 안되고 sfile로 갔다가 가야함.
                sfile = StringIO.StringIO()
                img.save(sfile, "JPEG")
                f_thumb.write(sfile.getvalue())
                f_thumb.close()
            except:
                ''

        else:
            if self.url:
                img = Image.open(StringIO.StringIO(self.url.read()))
                if img.mode != 'RGB':
                    img = img.convert('RGB')
                if self.url.width > 1000:
                    img.thumbnail((self.url.width/2, self.url.height/2), Image.ANTIALIAS)

                output = StringIO.StringIO()
                img.save(output, format='JPEG', quality=80)
                output.seek(0)
                self.url = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" %self.url.name.split('.')[0], 'image/jpeg'
                                                , output.len, None)
                self.create_image_thumb()

        super(LookContents, self).save(*args, **kwargs)

    def create_image_thumb(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base
        try:
            # resize the original image and return url path of the thumbnail
            image = Image.open(StringIO.StringIO(self.url.read()))
            image.thumbnail((self.url.width / 2, self.url.height / 2), Image.ANTIALIAS)

            f_thumb = storage.open(thumb_file_path, "w")
            image.save(f_thumb, "JPEG")
            f_thumb.close()
            return "success"
        except:
            return "error"

    def get_thumb_url(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base
        if storage.exists(thumb_file_path):
            return storage.url(thumb_file_path)
        return self.url.url


class LookArea(models.Model):
    class Meta:
        db_table = 'look_area'
    look = models.ForeignKey(Look, db_constraint=False, related_name='areas')
    areaPos = models.CharField(max_length=100)
    areaWidth = models.FloatField()
    areaHeight = models.FloatField()
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')


class LookClassification(models.Model):
    class Meta:
        db_table = 'look_classification'
    look = models.OneToOneField(Look,
                                db_constraint=False,
                                primary_key=True,
                                unique=True,
                                related_name='classification')
    look_content_url = models.ImageField()
    is_checked = models.BooleanField(default=False)
    main_look = models.BooleanField(default=False)

    basic = models.BooleanField(default=False)
    modern = models.BooleanField(default=False)
    casual = models.BooleanField(default=False)
    ethnic = models.BooleanField(default=False)
    unique = models.BooleanField(default=False)
    feminie = models.BooleanField(default=False)
    girlish = models.BooleanField(default=False)
    unisex = models.BooleanField(default=False)
    impossible = models.BooleanField(default=False)
    main_look = models.BooleanField(default=False)
    insertDate = models.DateTimeField(auto_now_add=True)


class Item(models.Model):
    class Meta:
        db_table = 'item'
    look = models.ForeignKey(Look, db_constraint=False, related_name='items', null=True)
    shop = models.ForeignKey(Shop, db_constraint=False, related_name='shop_items', null=True)
    area = models.OneToOneField(LookArea, db_constraint=False, null=True)

    directpay_code = models.CharField(max_length=255, unique=True, null=True)
    directpay_cover_url = models.CharField(max_length=1024, null=True)

    forwardUrl = models.CharField(max_length=1024)
    name = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    price = models.IntegerField()
    formal_price = models.IntegerField(null=True)
    delivery_fee = models.IntegerField(null=True)

    desc = models.TextField()
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')
    isSellYN = models.CharField(max_length=1, default='N')

    direct_title = models.CharField(max_length=128, null=True)
    direct_desc = models.CharField(max_length=1024, null=True)
    direct_url = models.CharField(max_length=1024, null=True)
    direct_image = models.ForeignKey('ItemContents', db_constraint=False, null=True, related_name='item_direct')


class ItemOptions(models.Model):
    class Meta:
        db_table = 'item_options'
    item = models.ForeignKey(Item, db_constraint=False, related_name='options')
    size = models.CharField(null=False, max_length= 45)
    color = models.CharField(null=False, max_length= 45)
    add_price = models.IntegerField()
    sale_amount = models.IntegerField(default=0)
    total_amount = models.IntegerField()
    isDelYN = models.CharField(max_length=1, default='N')
    insertDate = models.DateTimeField(auto_now_add=True)
    updateDate = models.DateTimeField()


class ItemContents(models.Model):
    class Meta:
        db_table = 'item_contents'
    item = models.ForeignKey(Item, db_constraint=False, related_name='contents')
    type = models.CharField(max_length=10)
    file_name = models.CharField(max_length=255)
    url = models.ImageField(upload_to=image_directory_path)
    isDelYN = models.CharField(max_length=1, default='N')

    def save(self, *args, **kwargs):
        if self.url:
            img = Image.open(StringIO.StringIO(self.url.read()))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            if self.url.width > 1000:
                img.thumbnail((self.url.width/1.5, self.url.height/1.5), Image.ANTIALIAS)

            output = StringIO.StringIO()
            img.save(output, format='JPEG', quality=80)
            output.seek(0)
            self.url = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" %self.url.name.split('.')[0], 'image/jpeg'
                                            , output.len, None)
        super(ItemContents, self).save(*args, **kwargs)

    def create_image_thumb(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base

        try:
            # resize the original image and return url path of the thumbnail
            image = Image.open(StringIO.StringIO(self.url.read()))
            image.thumbnail((self.url.width / 2, self.url.height / 2), Image.ANTIALIAS)

            f_thumb = storage.open(thumb_file_path, "w")
            image.save(f_thumb, "JPEG")
            f_thumb.close()
            return "success"
        except:
            return "error"

    def get_thumb_url(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base
        if storage.exists(thumb_file_path):
            return storage.url(thumb_file_path)
        return self.url.url


class ItemTag(models.Model):
    class Meta:
        db_table = 'item_tag'
    item = models.ForeignKey(Item, db_constraint=False, related_name='tags')
    name = models.CharField(max_length=45)
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')


class ItemIdentity(models.Model):
    class Meta:
        db_table = 'item_identity'
    item = models.ForeignKey(Item, db_constraint=False, related_name='identities')
    name = models.CharField(max_length=45)
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')


class ImageUpload(models.Model):
    name = models.CharField(max_length=45)


class LookByDate(models.Model):
    printDate = models.CharField(max_length=10)
    countEvents = models.IntegerField()
    topUrl = models.CharField(max_length=1024)


class LogForView(models.Model):
    value = models.CharField(max_length=1024)
    count = models.CharField(max_length=100)


class TagSearch(models.Model):
    count = models.IntegerField()
    name = models.CharField(max_length=45)


class RecommendTag(models.Model):
    class Meta:
        db_table = 'recommend_tag'
    name = models.CharField(max_length=45)
    isCelebYN = models.CharField(max_length=1, default='N')
    desc = models.CharField(max_length=1024, null=True)
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')
    url = models.ImageField(upload_to=image_directory_path)
    isMainShopYN = models.CharField(max_length=1, default='N')
    isShowYN = models.CharField(max_length=1, default='Y')
    sortNo = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if self.url:
            img = Image.open(StringIO.StringIO(self.url.read()))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            if self.url.width > 1000:
                img.thumbnail((self.url.width/1.5, self.url.height/1.5), Image.ANTIALIAS)

            output = StringIO.StringIO()
            img.save(output, format='JPEG', quality=80)
            output.seek(0)
            self.url = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" %self.url.name.split('.')[0], 'image/jpeg'
                                            , output.len, None)
        super(RecommendTag, self).save(*args, **kwargs)
        self.create_image_thumb()

    def create_image_thumb(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base

        try:
            # resize the original image and return url path of the thumbnail
            image = Image.open(StringIO.StringIO(self.url.read()))
            image.thumbnail((self.url.width / 2, self.url.height / 2), Image.ANTIALIAS)

            f_thumb = storage.open(thumb_file_path, "w")
            image.save(f_thumb, "JPEG")
            f_thumb.close()
            return "success"
        except:
            return "error"

    def get_thumb_url(self):
        if not self.url:
            return ""
        file_path = self.url.name
        filename_base, filename_ext = os.path.splitext(file_path)
        thumb_file_path = "%s_thumb.jpg" % filename_base
        if storage.exists(thumb_file_path):
            return storage.url(thumb_file_path)
        return self.url.url


class DailyLookCover(models.Model):
    class Meta:
        db_table = 'daily_look_cover'
    user = models.ForeignKey(User, db_constraint=False)
    desc = models.CharField(max_length=512)
    tagName = models.CharField(max_length=45)
    insertDate = models.DateTimeField(auto_now_add=True)
    isDelYN = models.CharField(max_length=1, default='N')
    url = models.ImageField(upload_to=image_directory_path)
    webUrl = models.ImageField(upload_to=image_directory_path)
    postDate = models.DateTimeField()

    def save(self, *args, **kwargs):
        if self.url:
            img = Image.open(StringIO.StringIO(self.url.read()))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            if self.url.width > 1000:
                img.thumbnail((self.url.width/1.5, self.url.height/1.5), Image.ANTIALIAS)

            output = StringIO.StringIO()
            img.save(output, format='JPEG', quality=80)
            output.seek(0)
            self.url = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" %self.url.name.split('.')[0], 'image/jpeg'
                                            , output.len, None)
        super(DailyLookCover, self).save(*args, **kwargs)


class LookUserLike(models.Model):
    class Meta:
        db_table = 'look_user_like'
        unique_together = (('user', 'look'),)

    user = models.ForeignKey(User, db_constraint=False)
    look = models.ForeignKey(Look, db_constraint=False, related_name='likes')
    insertDate = models.DateTimeField(auto_now_add=True)


class LookWish(models.Model):
    class Meta:
        db_table = 'look_wish'
        unique_together = (('user', 'look'),)

    user = models.ForeignKey(User, db_constraint=False)
    look = models.ForeignKey(Look, db_constraint=False, related_name='wishes')
    insertDate = models.DateTimeField(auto_now_add=True)
    put_from = models.CharField(max_length=45, null=True)



#branch shoppin-8
#item size
class ItemSize(models.Model):
    class Meta:
        db_table = 'item_size'

    item = models.ForeignKey(Item, db_constraint=False, related_name='sizes')
    name = models.CharField(null=False, max_length= 45)
    shoulder = models.CharField(default='', max_length=4, null=False)
    bust = models.FloatField(default=None, null=True)
    arm_length = models.CharField(default='', max_length=4, null=False)
    waist = models.FloatField(default=None,null=True)
    hip = models.FloatField(default=None,null=True)
    hern = models.FloatField(default=None,null=True)
    total_length = models.FloatField(default=None,null=True)
    rise = models.FloatField(default=None, null=True)
    thigh = models.FloatField(default=None, null=True)


class ItemColor(models.Model):
    class Meta:
        db_table = 'item_color'
    item = models.ForeignKey(Item, db_constraint=False, related_name='colors')
    name = models.CharField(null=False, max_length=45)
    insertDate = models.DateTimeField(auto_now_add=True)


#item_fabric
class ItemFabric(models.Model):
    class Meta:
        db_table = 'item_fabric'

    item = models.OneToOneField(Item, db_constraint=False,related_name='fabric')
    material = models.CharField(max_length=1024)
    #비침, 있음 : 2, 약간 : 1, 없음 : 0
    sheerness = models.IntegerField(default=None)
    #두께감, 얇음 : 0, 중간 : 1, 두꺼움 : 2
    thickness = models.IntegerField(default=None)
    #신축성, 매우좋음 : 3, 좋음 : 2, 약간 : 1, 없음 : 0
    elasticity = models.IntegerField(default=None)
    #계절감, 봄 : 0, 여름 : 1, 가을 : 2, 겨울 : 3, 봄/여름 : 4, 봄/여름/가을 : 5, 봄/가을: 6, 봄/가을/겨울 : 7, 가을/겨울 : 8, 봄/여름/가을/겨울: 9
    season = models.IntegerField(default = None)
    #안감, 없음 : 0, 있음 : 1, 기모 : 2
    lining = models.IntegerField(default=None)

    # 재질 세탁 가능 여부
    washing_hand_yn = models.CharField(default='N', max_length=1)
    washing_machine_yn = models.CharField(default='N', max_length=1)
    washing_dry_yn = models.CharField(default='N', max_length=1)
    washing_spin_yn = models.CharField(default='N', max_length=1)

    extra_desc = models.CharField(max_length=1024, null=True)


class OrderAddress(models.Model):
    class Meta:
        db_table = 'order_address'
    user = models.ForeignKey(User, db_constraint=False, related_name='addresses', null=True)
    name = models.CharField(max_length=255, null=True)
    mobile = models.CharField(max_length=255, null=True)
    zonecode = models.CharField(max_length=45, null=False)
    addr_def = models.CharField(max_length=255, null=False)
    addr_detail = models.CharField(max_length=45, null=False)
    order_message = models.CharField(max_length=1024, null=True)
    email = models.CharField(max_length=1024, null=True)

    is_def_yn = models.CharField(max_length=1, null=True)


class Order(models.Model):
    class Meta:
        db_table = 'order'

    order_no = models.CharField(max_length=255, null=False, unique=True)
    # 0.구매취소 : CANCEL_BUY
    # 1.구매대기 :  WAIT_BUY
    # 2.결제대기 : WAIT_PAY
    # 3.결제확인 : COM_PAY
    # 5. 결제오류 : ERR_PAY
    # 6. 결제취소 : CANCEL_PAY
    # 7. 결제실패 : FAIL_PAY
    shop = models.ManyToManyField(Shop, through='OrderShopRelation')
    order_delivery = models.ManyToManyField('OrderDelivery', through='OrderShopRelation')
    status_code = models.CharField(max_length=10, null=False)
    user = models.ForeignKey(User, db_constraint=False, related_name='orders', null=True)
    receive_address = models.ForeignKey(OrderAddress, db_constraint=False)
    isDelYN = models.CharField(max_length=1, default='N')

    ori_goods_price = models.IntegerField(null=False)
    ori_send_cost = models.IntegerField(null=False)

    receive_point = models.IntegerField(null=True)

    pay_total_price = models.IntegerField(null=False)
    pay_coupon = models.IntegerField(null=True, default=0)
    pay_point = models.IntegerField(null=True, default=0)

    pay_send_cost = models.IntegerField(null=True)
    pay_send_coupon = models.IntegerField(null=True)

    insertDate = models.DateTimeField(auto_now_add=True)
    updatedStatusDate = models.DateTimeField(null=True)

    pay_method = models.CharField(max_length=45)
    imp_uid = models.CharField(max_length=1024, null=True)
    paid_amount = models.IntegerField()

    pay_time = models.DateTimeField()
    pay_name = models.CharField(max_length=255, null=True)
    pg_provider = models.CharField(max_length=45, null=True)
    pg_tid = models.CharField(max_length=1024, null=True)
    pay_card_apply_num = models.CharField(max_length=1024, null=True)
    pay_vbank_num = models.CharField(max_length=1024, null=True)
    pay_vbank_name = models.CharField(max_length=45, null=True)
    pay_vbank_holder = models.CharField(max_length=45, null=True)
    pay_vbank_date = models.CharField(max_length=45, null=True)
    pay_receipt_url = models.CharField(max_length=1024, null=True)

    pay_error_code = models.CharField(max_length=100, null=True)
    pay_error_msg = models.CharField(max_length=1024, null=True)

    pay_escrow_yn = models.CharField(max_length=1, null=True)
    pay_cash_yn = models.CharField(max_length=1, null=True)
    pay_cash_info = models.TextField(null=True)
    pay_cash_no = models.CharField(max_length=255, null=True)

    is_sms_pushed_yn = models.CharField(max_length=1, null=False, default='N')

    #비회원 정보
    non_user_name = models.CharField(max_length=45, null=True)
    non_user_phone = models.CharField(max_length=255, null=True)

    def save(self, *args, **kwargs):
        order = Order.objects.filter(id=self.id).first()

        if order:
            if order.status_code != None:
                if self.status_code != order.status_code:
                    self.updatedStatusDate = datetime.now()
            else:
                if self.status_code != None:
                    self.updatedStatusDate = datetime.now()

        super(Order, self).save(*args, **kwargs)


class OrderShopRelation(models.Model):
    class Meta:
        db_table = 'order_and_shop_relation'
        unique_together = (('order', 'shop'), ('order', 'order_delivery'))

    order = models.ForeignKey(Order, db_constraint=False)
    order_delivery = models.ForeignKey('OrderDelivery', db_constraint=False, null=True)
    shop = models.ForeignKey(Shop, db_constraint=False)
    insertDate = models.DateTimeField(auto_now_add=True)


class ItemClickLog(models.Model):
    class Meta:
        db_table = 'item_click_log'
    item = models.ForeignKey(Item, db_constraint=False)
    user = models.ForeignKey(User, db_constraint=False)
    location = models.CharField(max_length=1024)
    insertDate = models.DateTimeField(auto_now_add=True)


class OrderDelivery(models.Model):
    class Meta:
        db_table = 'order_delivery'
    company = models.CharField(max_length=255, null=True)
    invoice = models.CharField(max_length=255, null=True)
    invoice_time = models.DateTimeField()
    # 1. 배송취소 : CANCEL_DELIV
    # 2. 배송준비 : WAIT_DELIV
    # 3. 배송중 : ING_DELIV
    # 4. 배송완료 : COM_DELIV
    status_code = models.CharField(max_length=20, null=True)
    insertDate = models.DateTimeField(auto_now_add=True)
    updateDate = models.DateTimeField()
    updatedStatusDate = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        #get으로 받을시에 pk값 없으면 exception발생 so filter로 교체
        order_delivery = OrderDelivery.objects.filter(pk=self.pk).first()

        if order_delivery:
            if order_delivery.status_code != None:
                if self.status_code != order_delivery.status_code:
                    self.updatedStatusDate = datetime.now()
            else:
                if self.status_code != None:
                    self.updatedStatusDate = datetime.now()

        super(OrderDelivery, self).save(*args, **kwargs)

class OrderCart(models.Model):
    class Meta:
        db_table = 'order_cart'
    order = models.ForeignKey(Order, db_constraint=False, related_name='carts')

    # 1. 카트 : CART
    # 2. 직접구매 : DIRECT
    # 3. 결제대기 : WAIT_PAY
    # 4. 결제완료 : COM_PAY

    # 하위 버전의 order cart에서는 배송 상태가 있음.
    # 4.배송준비 : WAIT_DELIV
    # 5.배송중 : 	ING_DELIV
    # 6. 배송완료 : COM_DELIV
    # 7.취소신청 : ASK_CANCEL
    # 8.취소완료 : COM_CANCEL
    # 9.반품신청 : ASK_REFUND
    # 10.반품완료 : COM_REFUND

    # 1.취소신청 : ASK_CANCEL
    # 2.취소완료 : COM_CANCEL
    # 3.반품신청 : ASK_REFUND
    # 4.반품완료 : COM_REFUND
    status_code = models.CharField(max_length=20, null=False)

    user = models.ForeignKey(User, db_constraint=False, null=True)
    item = models.ForeignKey(Item, db_constraint=False)

    color = models.ForeignKey(ItemColor, db_constraint=False, null=True)
    size = models.ForeignKey(ItemSize, db_constraint=False, null=True)
    option = models.ForeignKey(ItemOptions, db_constraint=False, null=True)

    delivery = models.ForeignKey(OrderDelivery, db_constraint=False, null=True)
    quantity = models.IntegerField()
    insertDate = models.DateTimeField(auto_now_add=True)
    updatedStatusDate = models.DateTimeField(null=True)
    updateDate = models.DateTimeField()
    isDelYN = models.CharField(max_length=1,default='N')

    def save(self, *args, **kwargs):
        #get으로 받을시에 pk값 없으면 exception발생 so filter로 교체
        order_cart = OrderCart.objects.filter(pk=self.pk).first()

        if order_cart:
            if order_cart.status_code != None:
                if self.status_code != order_cart.status_code:
                    self.updatedStatusDate = datetime.now()
            else:
                if self.status_code != None:
                    self.updatedStatusDate = datetime.now()

        super(OrderCart, self).save(*args, **kwargs)


class PaymentMethod(models.Model):
    class Meta:
        db_table = 'payment_method'

    isCreditYN = models.CharField(max_length=1, default='N')
    isDirectYN = models.CharField(max_length=1, default='Y')
    isKakaoYN = models.CharField(max_length=1, default='N')
    isTossYN = models.CharField(max_length=1, default='N')

    insertDate = models.DateTimeField(auto_now_add=True)


class PayPointLog(models.Model):
    class Meta:
        db_table = 'pay_point_log'
    user = models.ForeignKey(User, db_constraint=False)
    order = models.ForeignKey(Order, db_constraint=False, null=True)
    point = models.IntegerField(null=True),
    insertDate = models.DateTimeField(auto_now_add=True)


class StyleOlympic(models.Model):
    class Meta:
        db_table = 'style_olympic'
    name = models.CharField(max_length=1024, null=False)
    share_fb_count = models.IntegerField(null=False, default=0)
    share_kt_count = models.IntegerField(null=False, default=0)
    desc = models.TextField()
    insertDate = models.DateTimeField(auto_now_add=True)
    url = models.ImageField(upload_to=image_directory_path)


class StyleOlympicStep(models.Model):
    class Meta:
        db_table = 'style_olympic_step'
    step_no = models.IntegerField(null=False)
    olympic = models.ForeignKey(StyleOlympic, db_constraint=False, related_name='steps')
    name = models.CharField(max_length=1024, null=False)

    look_amount = models.IntegerField(null=False, default=0)
    look_select_amount = models.IntegerField(null=False, default=0)
    insertDate = models.DateTimeField(auto_now_add=True)


class StyleOlympicResult(models.Model):
    class Meta:
        db_table = 'style_olympic_result'
    olympic = models.ForeignKey(StyleOlympic, db_constraint=False, related_name='results')
    name = models.CharField(max_length=45, null=False)
    display_title = models.CharField(max_length=128, null=True)
    display_middle_title = models.CharField(max_length=128, null=True)
    display_desc = models.TextField()
    display_percentage = models.IntegerField()
    insertDate = models.DateTimeField(auto_now_add=True)
    url = models.ImageField(upload_to=image_directory_path)


class StyleOlympicUserResult(models.Model):
    class Meta:
        db_table = 'style_olympic_user_result'
    user = models.ForeignKey(User, db_constraint=False, null=True)
    result = models.ForeignKey(StyleOlympicResult, db_constraint=False, null=True)
    session_key = models.CharField(max_length=1024, null=True)
    insertDate = models.DateTimeField(auto_now_add=True)


class StyleOlympicUserResultLook(models.Model):
    class Meta:
        db_table = 'style_olympic_user_result_look'
    olympic = models.ForeignKey(StyleOlympic, db_constraint=False)
    olympic_step = models.ForeignKey(StyleOlympicStep, db_constraint=False, related_name='user_results')
    result = models.ForeignKey(StyleOlympicResult, db_constraint=False)
    look = models.ForeignKey(Look, db_constraint=False)
    user = models.ForeignKey(User, db_constraint=False, null=True)
    insertDate = models.DateTimeField(auto_now_add=True)

    # def user_directory_path(instance, filename):
    #     # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    #     return 'uploads/%Y/%m/%d/%Y%m%d_%H%M%S_%f'


class DeliveryCompany(models.Model):
    class Meta:
        db_table = 'delivery_company'

    code = models.CharField(max_length=10)
    name = models.CharField(max_length=45)