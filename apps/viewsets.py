# -*- coding: utf-8 -*-

# ViewSets define the view behavior.
from collections import OrderedDict
import datetime
from django.core.cache import cache
from rest_framework import parsers, renderers
from rest_framework.filters import OrderingFilter

from apps.permissions import UserPermission
from apps.restutil import RestaurantFilter
from apps.serializers import *
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.db import models
from django.db.models import Value, CharField, Count, Avg
from django.utils.encoding import force_text

import django_filters
from apps.models import LsUser, LsRestaurants, LsRestaurantsMenu, LsRestaurantsReview, LsRestaurantsReviewPhoto, \
    LsRestaurantsReviewTag, LsOrder, LsRestaurantsMenuCategory, LsRestaurantsCategoryMaster
from rest_framework import viewsets, pagination, status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import authentication_classes, permission_classes, detail_route, list_route
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView
from rest_framework_swagger import renderers


class LsValidation(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'A server error occurred.'

    def __init__(self, detail, field, status_code):
        if status_code is not None:self.status_code = status_code
        if detail is not None:
            self.detail = {field: force_text(detail)}
        else: self.detail = {'detail': force_text(self.default_detail)}


class UserViewSet(viewsets.ModelViewSet):
    queryset = LsUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserPermission,)

    def create(self, request, *args, **kwargs):
        # Validating our serializer from the UserRegistrationSerializer
        serializer = UserRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        user.auth_token = token

        # Everything's valid, so send it to the UserSerializer
        model_serializer = UserSerializer(user)
        return Response(model_serializer.data)

    @list_route(methods=['get'])
    def get_user_distance(self, request, pk=None):
        user = self.get_queryset().get(id=request.user.id)
        if not user.use_mile:
            user_distance = [{"name" : "300m", "value": "300", "unit": "meter"},
                             {"name" : "500m", "value": "500", "unit": "meter"},
                             {"name" : "750m", "value": "750", "unit": "meter"}]
        else :
            user_distance = [{"name" : "0.1mile", "value": "0.1", "unit": "mile"},
                             {"name" : "0.3mile", "value": "0.3", "unit": "mile"},
                             {"name" : "0.5mile", "value": "0.5", "unit": "mile"}]

        return Response(user_distance)

    @list_route(methods=['put'])
    def set_user_option(self, request, pk=None):
        user = self.get_queryset().get(id=request.user.id)
        use_mile = request.data.get('use_mile', None)
        use_push = request.data.get('use_push', None)

        if use_push:
            user.use_push = use_push

        if use_mile:
            user.use_mile = use_mile

        if use_push or use_mile:
            user.save()

        return Response(UserSerializer(user).data)

    @list_route(methods=['post'])
    def set_password(self, request, pk=None):
        user = self.get_queryset().get(id=request.user.id)
        serializer = PasswordSerializer(data=request.data)
        if serializer.is_valid():
            if not self.get_object().check_password(serializer.data.get("old_password")):
                return Response("Wrong password", status=status.HTTP_400_BAD_REQUEST)

            user.set_password(serializer.data['new_password'])
            user.save()
            return Response({'status': 'password set'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated,))
class RestaurantViewSet(viewsets.ModelViewSet):
    filter_class = RestaurantFilter
    serializer_class = RestaurantSerializer
    # pagination_class = RestaurantPagination

    @list_route(methods=['get'])
    def featured(self, request):
        queryset = self.get_queryset().filter(id__in=('7', '8', '3'))
        rest_serializer = RestaurantSerializer(queryset, many=True)
        return Response(rest_serializer.data)

    @list_route(methods=['get'])
    def categories(self, request):
        query = LsRestaurantsCategoryMaster.objects.all()
        category_serializer = RestaurantCategorySerializer(query, many=True)
        return Response(category_serializer.data)

    @list_route(methods=['get'])
    def autocomplete(self, request):
        query = self.request.query_params.get('query', None)
        if query:
            # cache 가져오기
            menu_data = cache.get("lineskipper:menu-name")
            rest_data = cache.get("lineskipper:rest-name")

            menu_data_result = [menu for menu in menu_data if query in menu["name"]]
            rest_data_result = [rest for rest in rest_data if query in rest["name"]]

            menu_serializer = SearchSerializer(menu_data_result, many=True)
            rest_serializer = RestaurantSearchSerializer(rest_data_result, many=True)
            return Response(menu_serializer.data + rest_serializer.data)
        return Response({})

    @list_route(methods=['get'])
    def search_by_menu_name(self, request):
        query = self.request.query_params.get('query', None)
        if query:
            queryset = self.get_queryset().filter(menus__name__search=query)
            rest_serializer = RestaurantSerializer(queryset, many=True)
            return Response(rest_serializer.data)
        return Response({})

    @list_route(methods=['get'])
    def search(self, request):
        query = self.request.query_params.get('query', None)
        if query:
            queryset = self.get_queryset().filter(name__search=query)
            rest_serializer = RestaurantSerializer(queryset, many=True)
            return Response(rest_serializer.data)
        return Response({})

    @list_route(methods=['get', 'post'])
    def favorite(self, request):
        if request.method == 'GET':
            queryset = self.get_queryset().filter(favorites__user_id=request.user.id)
            rest_serializer = RestaurantSerializer(queryset, many=True)
            return Response(rest_serializer.data)
        else:  # request.method == 'POST':
            rest_id = request.data.get('rest_id', None)
            if rest_id:
                rest_favorite, created = LsUserRestaurantsFavorite.objects.get_or_create(user_id=request.user.id, rest_id=rest_id)
                if not created:
                    rest_favorite.delete()
                queryset = self.get_queryset().filter(id=rest_id)
                rest_serializer = RestaurantSerializer(queryset, many=True)
                return Response(rest_serializer.data)
            return Response({})

    def filter_queryset(self, queryset):
        queryset = super(RestaurantViewSet, self).filter_queryset(queryset)
        ordering = self.request.query_params.get('ordering', None)
        if ordering:
            if ordering == 'distance':
                queryset = queryset.order_by('distance')
            if ordering == 'rating':
                queryset = queryset.order_by('-rating')
            if ordering == 'favorite_count':
                queryset = queryset.order_by('-favorite_count')
            if ordering == 'order':
                queryset = queryset.order_by('-order_count')
            if ordering == 'review':
                queryset = queryset.order_by('-review_count')
            if ordering == '-price':
                queryset = queryset.order_by('-avg_price')
            if ordering == 'price':
                queryset = queryset.order_by('avg_price')

        return queryset

    def get_queryset(self):
        # 몇분안에 가능한지 포함시킬것
        # search filter
        # ordering filter

        longitude = self.request.query_params.get('longitude', None)
        latitude = self.request.query_params.get('latitude', None)

        distance_lte = int(self.request.query_params.get('max_distance', 0.0)) / 100000

        now = datetime.datetime.now()
        tomorrow = now + datetime.timedelta(days=1)
        weekday_name = now.strftime('%A')
        tom_weekday_name = tomorrow.strftime('%A')

        now_date = now.strftime('%Y-%m-%d')
        now_time = now.strftime('%H:%M')

        queryset = LsRestaurants.objects\
            .annotate(order_count=Count('orders', distinct=True))\
            .annotate(review_count=Count('reviews', distinct=True)) \
            .annotate(avg_price=Avg('menus__display_price', distinct=True)) \
            .annotate(rating=Avg('reviews__rating', distinct=True))\
            .annotate(favorite_count=Count('favorites', distinct=True))\
            .extra(select={'is_favorited': """
                SELECT COUNT(*) FROM  LS_USER_RESTAURANTS_FAVORITE B
                  WHERE B.rest_id = `LS_RESTAURANTS`.id AND B.user_id = %s
            """}, select_params=(self.request.user.id,))\
            .extra(select={'tomorrow_open_hour': """
                SELECT """ + tom_weekday_name + """_open_hour
                FROM LS_RESTAURANTS_OPERATION_HOUR A
                WHERE A.rest_id = `LS_RESTAURANTS`.id
            """})\
            .extra(select={'is_online' : """
                    SELECT C.is_activate * C.is_not_holiday AS Result FROM (
                    SELECT B.id,
                    (SELECT COUNT(*) AS is_activate FROM
                        LS_RESTAURANTS_OPERATION_HOUR A
                    WHERE A.rest_id = B.id
                            AND (""" + weekday_name + """_open_hour < %s
                            AND """  + weekday_name + """_close_hour > %s
                            AND """  + weekday_name + """_open_hour != 'CLOSED'
                            AND """  + weekday_name + """_close_hour != 'CLOSED')

                          AND
                           !( A.break_start_hour is not null
                            AND A.break_end_hour is not null
                            AND A.break_start_hour < %s
                            AND A.break_end_hour > %s)) as is_activate
                        ,
                    (SELECT (CASE COUNT(*)
                                WHEN 1 THEN 0
                            ELSE 1 END) FROM LS_RESTAURANTS_HOLIDAY C
                        WHERE C.rest_id = B.id
                              AND C.holiday = %s) as is_not_holiday
                            FROM LS_RESTAURANTS B )
                            C WHERE C.id = `LS_RESTAURANTS`.id
                            """},
                   select_params=(now_time, now_time, now_time, now_time, now_date,),)\
            .exclude(is_activate=0) \
            .prefetch_related('op_hours')\
            .prefetch_related('owners')\
            .prefetch_related('categories')\
            .distinct()

        if longitude and latitude:
            pnt = Point(float(longitude), float(latitude), srid=4326)
            queryset = queryset.annotate(dist=Distance('geo_point', pnt))
            if distance_lte > 0.0:
                queryset = queryset.filter(geo_point__distance_lte=(pnt, distance_lte))
        else:
            queryset = queryset.annotate(distance=Value('undefined', output_field=CharField()))

        return queryset


class RestaurantMenuViewSet(viewsets.ModelViewSet):
    serializer_class = RestaurantMenuSerializer

    def list(self, request, restaurants_pk=None):
        category_id = self.request.query_params.get('category_id', None)
        menus = self.get_queryset().filter(rest_id=restaurants_pk,
                                           category__id=category_id) \
                .extra(select={'recommended_count': """
                   SELECT COUNT(*) FROM  LS_ORDER_ITEM B
                  WHERE B.menu_id = `LS_RESTAURANTS_MENU`.id AND B.is_recommended = 1
                 """})

        serializer = RestaurantMenuSerializer(menus, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return LsRestaurantsMenu.objects.filter(is_hide=0)\
                        .prefetch_related('extras')\
                        .prefetch_related('addons')\
                        .select_related('category')


class RestaurantMenuCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = LsRestaurantsMenuCategory.objects.all()

    def list(self, request, restaurants_pk=None):
        menu_categories = LsRestaurantsMenuCategory.objects.raw('''
        SELECT DISTINCT B.id as id,
              B.name as name FROM LS_RESTAURANTS_MENU A
                INNER JOIN LS_RESTAURANTS_MENU_CATEGORY B
                ON A.category_id = B.id
                WHERE A.rest_id =''' + restaurants_pk)

        serializer = RestaurantMenuCategorySerializer(menu_categories, many=True)
        return Response(serializer.data)


class RestaurantReviewTagViewSet(viewsets.ModelViewSet):
    queryset = LsRestaurantsReviewTagMaster.objects.all()
    serializer_class = RestaurantReviewTagSerializer

    def list(self, request):
        rating = self.request.query_params.get('rating', None)
        review_tags = self.get_queryset()
        if rating:
            review_tags = review_tags.filter(rating=rating)
        serializer = RestaurantReviewTagSerializer(review_tags, many=True)
        return Response(serializer.data)


class RestaurantReviewViewSet(viewsets.ModelViewSet):
    queryset = LsRestaurantsReview.objects.all()
    serializer_class = RestaurantReviewSerializer
    filter_backends = (OrderingFilter,)
    ordering_fields = ('insert_dt', 'rating')
    ordering = ('-insert_dt',)

    def list(self, request, restaurants_pk=None):
        reviews = self.filter_queryset(self.get_queryset())\
            .filter(rest_id=restaurants_pk)\
            .prefetch_related('photos')\
            .prefetch_related('tags')\
            .select_related('user')

        photo_only = int(request.query_params.get('photo_only', 0))
        if photo_only:
            reviews = reviews.annotate(num_p=Count('photos')).filter(num_p__gt=0)

        serializer = RestaurantReviewSerializer(reviews, many=True)
        return Response(serializer.data)

    def create(self, request, restaurants_pk=None):
        serializer = RestaurantWriteReviewSerializer(data=request.data,
                                                    context={'rest_id': restaurants_pk,
                                                            'user_id': request.user.id})
        serializer.is_valid(raise_exception=True)
        review = serializer.save()

        photos = request.data.get('photos', None)
        if photos:
            photos = photos.split(',')
            for url in photos:
                photo = LsRestaurantsReviewPhoto(photo_url=url, review_id=review.id)
                photo.save()

        tags = request.data.get('tags', None)
        if tags:
            tags = tags.split(',')
            for tag in tags:
                tag_model = LsRestaurantsReviewTag(tag_id=tag, review_id=review.id)
                tag_model.save()

        recommend_item_ids = request.data.get('recommend_item_ids', None)
        if recommend_item_ids:
            LsOrderItem.objects.filter(id__in=recommend_item_ids.split(',')).update(is_recommended=True)

        return Response(RestaurantReviewSerializer(review).data)


@permission_classes((IsAuthenticated,))
class OrderGroupViewSet(viewsets.ModelViewSet):
    queryset = LsOrderGroup.objects.all()
    serializer_class = OrderGroupSerializer

    def list(self, request):
        order_grp = LsOrderGroup.objects.filter(user_id=request.user.id) \
            .prefetch_related('orders').order_by('-insert_dt')
        serializer = OrderGroupSerializer(order_grp, many=True)
        response = []
        for data in serializer.data:
            for item in data['orders']:
                response.append(item)

        return Response(response)

    def create(self, request):
        serializer = OrderGroupWriteSerializer(data=request.data, context={'user_id': request.user.id})
        serializer.is_valid(raise_exception=True)
        order_grp = serializer.save()

        orders = json.loads(request.data['orders'])
        for order in orders:
            order_serializer = OrderWriteSerializer(data=order, context={'order_grp_id': order_grp.id})
            order_serializer.is_valid(raise_exception=True)
            order_serializer.save()

        order_grp = OrderGroupSerializer(order_grp)
        return Response(order_grp.data)


@permission_classes((IsAuthenticated,))
class OrderViewSet(viewsets.ModelViewSet):
    queryset = LsOrder.objects.all()
    serializer_class = OrderSerializer

    @list_route(methods=['get'])
    def get_recent_order_from_rest(self,request):
        rest_id = request.query_params.get('rest_id', None)
        if rest_id:
            order = self.get_queryset().filter(rest_id=rest_id, order_group__user_id=request.user.id).order_by('-insert_dt')
            if order.count() > 0:
                return Response(OrderSerializer(order[0]).data)
            return Response({})
        return Response({})

    def list(self, request):
        order_grp = self.get_queryset().filter(user_id=request.user.id)
        serializer = OrderGroupSerializer(order_grp, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = OrderGroupWriteSerializer(data=request.data, context={'user_id': request.user.id})
        serializer.is_valid(raise_exception=True)
        order_grp = serializer.save()

        orders = json.loads(request.data['orders'])
        for order in orders:
            order_serializer = OrderWriteSerializer(data=order, context={'order_grp_id': order_grp.id})
            order_serializer.is_valid(raise_exception=True)
            order_serializer.save()

        order_grp = OrderGroupSerializer(order_grp)
        return Response(order_grp.data)


class AllergyViewSet(viewsets.ModelViewSet):
    queryset = LsAllergyMaster.objects.all()
    serializer_class = AllergySerializer


class TrendingView(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)

    def get(self, request, format=None):
        trending = ["춘천", "햄버거", "규동", "오사카"]
        # serializer = TrendingSerializer(data=trending)
        return Response(trending)


