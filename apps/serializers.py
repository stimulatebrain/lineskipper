from drf_extra_fields.geo_fields import PointField
from apps.models import *
from rest_framework import serializers
from rest_framework.compat import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework.parsers import JSONParser


class SearchSerializer(serializers.Serializer):
    feed_type = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    count = serializers.IntegerField(required=True)


class SimpleSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True)


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("Username"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class UserSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(read_only=True, max_length=40, allow_null=True)

    class Meta:
        model = LsUser
        fields = ('id', 'last_login', 'first_name', 'last_name', 'email', 'phone', 'fb_token', 'fb_refresh_date',
                  'type', 'recommend_user_id', 'use_mile', 'auth_token', 'use_push')

    def create(self, validated_data):
        user = LsUser.objects.create_user(**validated_data)
        return user


class OuterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsUser
        fields = ('email', 'first_name', 'last_name', 'type', 'thumbnail_url', 'use_push', 'use_mile')


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsUser
        fields = ('email', 'first_name', 'last_name', 'password')

    def validate_email(self, email):
        existing = LsUser.objects.filter(email=email).first()
        if existing:
            raise serializers.ValidationError("Someone with that email "
                                              "address has already registered. Was it you?")
        return email


class PasswordSerializer(serializers.Serializer):
    """
     Serializer for password change endpoint.
     """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class RestaurantCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LsRestaurantsCategoryMaster
        fields = ('id', 'name', 'group_name')


class RestaurantOperationHourSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        is_mon_opened = obj.monday_open_hour != "CLOSE"
        is_tue_opened = obj.tuesday_open_hour != "CLOSE"
        is_wed_opened = obj.wednesday_open_hour != "CLOSE"
        is_thu_opened = obj.thursday_open_hour != "CLOSE"
        is_fri_opened = obj.friday_open_hour != "CLOSE"
        is_sat_opened = obj.saturday_open_hour != "CLOSE"
        is_sun_opened = obj.sunday_open_hour != "CLOSE"
        return [
                {
                    'day_name': 'Monday',
                    'open_hour': obj.monday_open_hour,
                    'close_hour': obj.monday_close_hour,
                    'is_opened': is_mon_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Tuesday',
                    'open_hour': obj.tuesday_open_hour,
                    'close_hour': obj.tuesday_close_hour,
                    'is_opened': is_tue_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Wednesday',
                    'open_hour': obj.wednesday_open_hour,
                    'close_hour': obj.wednesday_close_hour,
                    'is_opened': is_wed_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Thursday',
                    'open_hour': obj.thursday_open_hour,
                    'close_hour': obj.thursday_close_hour,
                    'is_opened': is_thu_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Friday',
                    'open_hour': obj.friday_open_hour,
                    'close_hour': obj.friday_close_hour,
                    'is_opened': is_fri_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Saturday',
                    'open_hour': obj.saturday_open_hour,
                    'close_hour': obj.saturday_close_hour,
                    'is_opened': is_sat_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },
                {
                    'day_name': 'Sunday',
                    'open_hour': obj.sunday_open_hour,
                    'close_hour': obj.sunday_close_hour,
                    'is_opened': is_sun_opened,
                    'break_start_hour': obj.break_start_hour,
                    'break_end_hour': obj.break_end_hour,
                },


        ]


class RestaurantSerializer(serializers.ModelSerializer):
    owners = UserSerializer(many=True)
    op_hours = RestaurantOperationHourSerializer()
    geo_point = PointField(null=True)
    categories = RestaurantCategorySerializer(many=True)
    distance = serializers.SerializerMethodField()
    is_online = serializers.BooleanField(read_only=True)
    tomorrow_open_hour = serializers.CharField()

    logo_url = serializers.ImageField(allow_null=True, read_only=True)
    image_urls = serializers.StringRelatedField(many=True, read_only=True, allow_null=True)

    is_favorited = serializers.BooleanField(read_only=True)
    favorite_count = serializers.IntegerField(read_only=True)

    order_count = serializers.IntegerField(read_only=True, default=0)
    review_count = serializers.IntegerField(read_only=True, default=0)
    rating = serializers.FloatField(read_only=True, default=0)
    avg_price = serializers.FloatField(read_only=True, default=0)
    insert_dt = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    def get_distance(self, obj):
        dist = getattr(obj, "dist", None)
        if dist is None:
            return 'undefined'
        return int(dist * 100000)

    class Meta:
        model = LsRestaurants
        exclude = ('update_dt',)


class RestaurantSearchSerializer(serializers.ModelSerializer):
    feed_type = serializers.CharField(required=False, allow_null=True)

    class Meta:
        model = LsRestaurants
        fields = ('id', 'name', 'desc', 'rest_no', 'feed_type')


class RestaurantLightSerializer(serializers.ModelSerializer):
    geo_point = PointField(null=True)
    categories = RestaurantCategorySerializer(many=True)

    logo_url = serializers.ImageField(allow_null=True, read_only=True)
    image_urls = serializers.StringRelatedField(many=True, read_only=True, allow_null=True)
    insert_dt = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    def get_distance(self, obj):
        dist = getattr(obj, "dist", None)
        if dist is None:
            return 'undefined'
        return int(dist * 100000)

    class Meta:
        model = LsRestaurants
        exclude = ('update_dt', 'owners', 'insert_dt')


class RestaurantMenuCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LsRestaurantsMenuCategory
        exclude = ('insert_dt', 'update_dt',)


class RestaurantMenuExtraSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsRestaurantsMenuExtra
        exclude = ('update_dt', 'insert_dt', 'menu')


class RestaurantMenuAddonSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsRestaurantsMenuAddon
        exclude = ('update_dt', 'insert_dt', 'menu')


class RestaurantMenuSerializer(serializers.ModelSerializer):
    category = RestaurantMenuCategorySerializer()
    extras = RestaurantMenuExtraSerializer(many=True)
    addons = RestaurantMenuAddonSerializer(many=True)

    recommended_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = LsRestaurantsMenu
        exclude = ('insert_dt', 'update_dt', 'rest')


class RestaurantMenuForOrderSerializer(serializers.ModelSerializer):
    category = RestaurantMenuCategorySerializer()

    class Meta:
        model = LsRestaurantsMenu
        exclude = ('insert_dt', 'update_dt', 'rest')


class RestaurantReviewTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsRestaurantsReviewTagMaster
        exclude = ('update_dt', 'insert_dt')


class RestaurantReviewSerializer(serializers.ModelSerializer):
    photos = serializers.StringRelatedField(many=True, allow_null=True)
    tags = RestaurantReviewTagSerializer(many=True, allow_null=True)
    user = OuterUserSerializer(required=False)
    insert_dt = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = LsRestaurantsReview
        exclude = ('update_dt', 'rest')


class RestaurantWriteReviewSerializer(serializers.ModelSerializer):
    photos = serializers.CharField(required=False)
    tags = serializers.CharField(required=False)
    user = OuterUserSerializer(required=False)

    def create(self, validated_data):
        obj = LsRestaurantsReview.objects.create(user_id=self.context.get('user_id'),
                                                 rest_id=self.context.get('rest_id'),
                                                 rating=validated_data['rating'],
                                                 contents=validated_data['contents'])
        return obj

    class Meta:
        model = LsRestaurantsReview
        exclude = ('update_dt', 'rest')


class PromoSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsPromoMaster
        exclude = ('update_dt',)


class OrderItemAddonSerializer(serializers.ModelSerializer):
    class Meta:
        model = LsOrderItemAddon
        exclude = ('update_dt', 'insert_dt')


class OrderItemSerializer(serializers.ModelSerializer):
    menu = RestaurantMenuForOrderSerializer()
    extra = RestaurantMenuExtraSerializer()
    addons = RestaurantMenuAddonSerializer(many=True)
    allergies = serializers.StringRelatedField(many=True, read_only=True, allow_null=True)

    def create(self, validated_data):
        obj = LsOrderItem.objects.create(**validated_data)
        return obj

    class Meta:
        model = LsOrderItem
        exclude = ('update_dt', 'order',)


class OrderItemWriteSerializer(serializers.ModelSerializer):
    menu_id = serializers.IntegerField()
    extra_id = serializers.IntegerField(required=False)
    addons_id = serializers.ListField(child=serializers.IntegerField(), required=False)
    allergies_name = serializers.ListField(child=serializers.CharField(), required=False)

    def create(self, validated_data):
        addons_id = validated_data.get('addons_id', None)
        if addons_id:
            validated_data.pop('addons_id')

        allergies_name = validated_data.get('allergies_name', None)
        if allergies_name:
            validated_data.pop('allergies_name')

        obj = LsOrderItem.objects.create(order_id=self.context.get('order_id'),
                                         **validated_data)

        if addons_id:
            for addon_id in addons_id:
                LsOrderItemAddon.objects.create(addon_id=addon_id, item_id=obj.id)

        if allergies_name:
            for name in allergies_name:
                LsOrderItemAllergy.objects.create(name=name, item_id=obj.id)
        return obj

    class Meta:
        model = LsOrderItem
        exclude = ('update_dt', 'order',)
        depth = 2


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)
    rest = RestaurantLightSerializer()
    insert_dt = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = LsOrder
        exclude = ('update_dt', )


class OrderWriteSerializer(serializers.ModelSerializer):
    items = OrderItemWriteSerializer(many=True)
    rest_id = serializers.PrimaryKeyRelatedField(queryset=LsRestaurants.objects.all(), source='rest')

    def create(self, validated_data):
        items = validated_data.pop('items')
        obj = LsOrder.objects.create(order_group_id=self.context.get('order_grp_id'),
                                     **validated_data)
        obj.order_no = generate_order_no(obj.rest.rest_no)
        obj.save()

        for item in items:
            item = OrderItemWriteSerializer(data=item, context={'order_id': obj.id})
            item.is_valid(raise_exception=True)
            item.save()

        return obj

    class Meta:
        model = LsOrder
        exclude = ('update_dt', )
        depth = 1


class OrderGroupWriteSerializer(serializers.ModelSerializer):
    user = OuterUserSerializer(required=False)
    promo = PromoSerializer(required=False)
    orders = OrderWriteSerializer(many=True, required=False)

    def create(self, validated_data):
        validated_data.pop('orders')
        obj = LsOrderGroup.objects.create(
            user_id=self.context.get('user_id'),
            **validated_data)
        return obj

    class Meta:
        model = LsOrderGroup
        exclude = ('update_dt',)


class OrderGroupSerializer(serializers.ModelSerializer):
    user = OuterUserSerializer(required=False)
    promo = PromoSerializer(required=False)
    orders = OrderSerializer(many=True, required=True)
    insert_dt = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = LsOrderGroup
        exclude = ('update_dt', )


class AllergySerializer(serializers.ModelSerializer):
    class Meta:
        model = LsAllergyMaster
        exclude = ('update_dt', 'insert_dt')


class TrendingSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()



