# -*- encoding: utf-8 -*-

import logging
from apps.models import LsRestaurantsMenu, LsRestaurants
from apps.serializers import SimpleSerializer, RestaurantSerializer
from django.db.models import Count, Value, CharField

from celery.schedules import crontab
from celery.task import periodic_task
from django.core.cache import cache

logger = logging.getLogger(__name__)


@periodic_task(run_every=(crontab(minute='*/1')), name="autocomplete_mining_task", ignore_result=True)
def tag_mining_task():
    # 메뉴 검색하기
    menus = LsRestaurantsMenu.objects.all()\
        .values('name') \
        .annotate(count=Count('name')) \
        .annotate(feed_type=Value('menu', output_field=CharField()))
    menu_serializer = SimpleSerializer(menus, many=True)

    # 레스토랑 검색하기
    rests = LsRestaurants.objects.all() \
        .exclude(is_activate=0) \
        .annotate(feed_type=Value('restaurants', output_field=CharField()))
    rest_serializer = RestaurantSerializer(rests, many=True)

    cache.set("lineskipper:menu-name", menu_serializer.data, timeout=None)
    cache.set("lineskipper:rest-name", rest_serializer.data, timeout=None)