from apps.models import LsRestaurants
import django_filters
from rest_framework import pagination


class RestaurantPagination(pagination.CursorPagination):
    page_size = 9999
    ordering = '-insert_dt'


class CharInFilter(django_filters.BaseInFilter, django_filters.CharFilter):
    pass


class RestaurantFilter(django_filters.FilterSet):
    categories = CharInFilter(name='categories__name', lookup_expr='in')

    class Meta:
        model = LsRestaurants
        fields = ['categories']