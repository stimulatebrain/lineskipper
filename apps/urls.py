# -*- coding: utf-8 -*-
from apps.views import LsObtainAuthToken
from django.apps import apps

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.http import HttpResponse
from django.views.generic import TemplateView

# Serializers define the API representation.
from apps import models
from apps.viewsets import UserViewSet, RestaurantViewSet, RestaurantMenuViewSet, RestaurantReviewViewSet, \
    RestaurantMenuCategoryViewSet, OrderViewSet, TrendingView, OrderGroupViewSet, RestaurantReviewTagViewSet, \
    AllergyViewSet
from rest_framework.documentation import include_docs_urls
from rest_framework_nested import routers
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='LineSkipper API')


# Routers provide an easy way of automatically determining the URL conf.
from rest_framework.authtoken import views

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, base_name='users')
router.register(r'restaurants', RestaurantViewSet, base_name='restaurants')
router.register(r'orders', OrderViewSet, base_name='orders')
router.register(r'order_groups', OrderGroupViewSet, base_name='order_groups')
router.register(r'review_tags', RestaurantReviewTagViewSet, base_name='review_tags')
router.register(r'allergies', AllergyViewSet, base_name='allergies')

menu_router = routers.NestedDefaultRouter(router, r'restaurants', lookup='restaurants')
menu_router.register(r'menus', RestaurantMenuViewSet, base_name='restaurants-menus')

menu_category_router = routers.NestedDefaultRouter(router, r'restaurants', lookup='restaurants')
menu_category_router.register(r'categories', RestaurantMenuCategoryViewSet, base_name='restaurants-categories')

review_router = routers.NestedDefaultRouter(router, r'restaurants', lookup='restaurants')
review_router.register(r'reviews', RestaurantReviewViewSet, base_name='restaurants-reviews')
# router.register(r'')

admin.site.site_header = 'Line Skipper Administration'
admin.site.index_title = 'Line Skipper'
admin.site.site_title = 'Line Skipper from adminsitration'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^sign-in/', LsObtainAuthToken.as_view()),
    url(r'^docs/', schema_view),

    # url(r'^manage/', include(url_main)),
    # url(r'^manage/order/', include(url_order)),
    # url(r'^manage/item/', include(url_item)),
    # url(r'^manage/info/', include(url_shop)),
    # url(r'^manage/insta/', include(url_insta)),

    # url(r'^manage/login/$', views.VendorLoginView.as_view(), name='login'),
    # url(r'^manage/logout/$', views.admin_logout, name='logout'),
    #
    #
    # url(r'^manage/join/$', views.VendorJoinView.as_view(), name='join'),
    # url(r'^manage/email_cert/$', views.VendorEmailCertificateView.as_view(), name='email_cert'),
    # url(r'^manage/email_retry/$', views.email_cert_retry),
    # url(r'^manage/join_detail/$', views.VendorJoinDetailView.as_view(), name='join_detail'),

    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^robots.txt$', lambda request: HttpResponse("User-agent: *\nDisallow: /static/*", content_type="text/plain")),
    url(r'^sitemap\.xml$', TemplateView.as_view(template_name='landing/sitemap.xml', content_type='text/xml')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^', include(router.urls)),
    url(r'^', include(menu_router.urls)),
    url(r'^', include(review_router.urls)),
    url(r'^', include(menu_category_router.urls)),
    url(r'^trending/$', TrendingView.as_view()),

    #
    # url(r'^$', views_insta.InstaMainView.as_view(), name='insta_main'),

    # url(r'', include(url_user)),

    # url(r'^like/$', views_main.main_like),
    # url(r'^like_insert/$', views_main.main_like_insert),
    #
    # url(r'^logout/$', logout, {'next_page': '/'}),
    # url(r'^check-email/$', views_join.check_email),
    # url(r'^check-user/$', views_main.check_user_count),
    # url(r'^robots.txt$', lambda request: HttpResponse("User-agent: *\nDisallow: /static/*", content_type="text/plain")),
    # url(r'^sitemap\.xml$', TemplateView.as_view(template_name='sitemap.xml', content_type='text/xml')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
