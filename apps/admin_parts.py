from django import forms
from django.conf import settings
from django.contrib import admin
from nested_inline.admin import NestedTabularInline
from tabbed_admin import TabbedModelAdmin
from tabbed_admin.settings import JQUERY_UI_JS, JQUERY_UI_CSS


class NestedInlineReadOnly(NestedTabularInline):
    can_delete = False

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        result = list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))
        result.remove('id')
        return result


class InlineReadOnly(admin.TabularInline):
    can_delete = False

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        result = list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))
        result.remove('id')
        return result


class TabbedModelAdminForDjango2(TabbedModelAdmin):
    @property
    def media(self):
        """
        Overrides media class to add custom jquery ui if
        TABBED_ADMIN_USE_JQUERY_UI is set to True.
        """
        media = super(TabbedModelAdmin, self).media
        css = {'all': (JQUERY_UI_CSS, 'tabbed_admin/css/tabbed_admin.css')}

        extra = '' if settings.DEBUG else '.min'
        js = [
            'admin/js/vendor/jquery/jquery%s.js' % extra,
            'admin/js/jquery.init.js',
            JQUERY_UI_JS,
            'admin/js/core.js',
            'admin/js/admin/RelatedObjectLookups.js',
            'admin/js/actions%s.js' % extra,
            'admin/js/urlify.js',
            'admin/js/prepopulate%s.js' % extra,
            'admin/js/vendor/xregexp/xregexp%s.js' % extra,
            ]

        return forms.Media(css=css, js=js)