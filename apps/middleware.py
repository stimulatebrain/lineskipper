from django.http import QueryDict
from apps.models import LsUser

__author__ = 'jspark'


from django.utils.timezone import now


# class SetLastVisitMiddleware(object):
#     def process_response(self, request, response):
#         if request.user.is_authenticated():
#             # Update last visit time after request finished processing.
#             User.objects.filter(pk=request.user.pk).update(last_visit=now())
#         return response
#
#
# class HttpPostTunnelingMiddleware(object):
#     def process_request(self, request):
#         if request.META.has_key('HTTP_X_METHODOVERRIDE'):
#             http_method = request.META['HTTP_X_METHODOVERRIDE']
#             if http_method.lower() == 'put':
#                 request.method  = 'PUT'
#                 request.META['REQUEST_METHOD'] = 'PUT'
#                 request.PUT = QueryDict(request.body)
#             if http_method.lower() == 'delete':
#                 request.method  = 'DELETE'
#                 request.META['REQUEST_METHOD'] = 'DELETE'
#                 request.DELETE = QueryDict(request.body)
#         return None