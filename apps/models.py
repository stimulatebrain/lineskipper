# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
import datetime
import os
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.gis.db.models import PointField
from django.contrib.gis.db import models as models_gis
from django.db import models
from django.utils.crypto import get_random_string
from rest_framework.authtoken.models import Token


class Search(models.Lookup):
    lookup_name = 'search'

    def as_mysql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return 'MATCH (%s) AGAINST (%s IN BOOLEAN MODE)' % (lhs, rhs), params

models.CharField.register_lookup(Search)
models_gis.CharField.register_lookup(Search)



def image_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    filename = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f") + os.path.splitext(filename)[1]
    return 'uploads/' + datetime.datetime.now().strftime("%Y/%m/%d/") + filename


class UserManager(BaseUserManager):
    def create_user_naver(self, email, password, name, naver_id, naver_token, gender, age_range):
        user = self.model(email=UserManager.normalize_email(email), name=name, gender=gender,
                          age_range=age_range, naver_token=naver_token, naver_id=naver_id)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user_instagram(self, email, password, name, insta_id, insta_token):
        user = self.model(email=UserManager.normalize_email(email), name=name, insta_id = insta_id, insta_token = insta_token)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user_facebook(self, email, password, name, fb_id, gender, age_range, fb_token):
        user = self.model(email=UserManager.normalize_email(email), name=name, fb_id=fb_id, gender=gender,
                          age_range=age_range, fb_token=fb_token)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, first_name, last_name, password, phone=None, fb_token=None, fb_refresh_date= None):
        user = self.model(email=UserManager.normalize_email(email), first_name=first_name, last_name=last_name,
                          phone=phone, fb_token=fb_token, fb_refresh_date=fb_refresh_date)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name, last_name):
        user = self.create_user(email, password=password, first_name=first_name, last_name=last_name)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class LsUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    credit = models.FloatField(default=0)
    email = models.CharField(unique=True, max_length=128)
    phone = models.CharField(max_length=128, blank=True, null=True)
    password = models.CharField(max_length=128)
    fb_token = models.CharField(max_length=45, blank=True, null=True)
    fb_refresh_date = models.CharField(max_length=45, blank=True, null=True)
    type = models.CharField(max_length=10, default='CLIENT')
    recommend_user_id = models.IntegerField(blank=True, null=True)
    use_mile = models.BooleanField(default=False)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    use_push = models.BooleanField(default=False)

    # favorite_rests = models.ManyToManyField('LsRestaurants', through='LsUserRestaurantsFavorite')
    # groups = models.ManyToManyField()

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)
    thumbnail_url = models.ImageField(null=True, upload_to=image_directory_path)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name", "password"]

    objects = UserManager()

    def get_short_name(self):
        return self.email

    class Meta:
        managed = False
        db_table = 'LS_USER'
        verbose_name = 'User'
        verbose_name_plural = 'User'


def generate_order_no(rest_no):
    date_search = datetime.date.today()
    day_str = date_search.strftime('%y%m%d')
    count = LsOrder.objects\
                .filter(rest__rest_no=rest_no,
                        insert_dt__gt=date_search).count() + 1
    return rest_no + day_str + '%05d' % count


class LsOrderGroup(models.Model):
    promo = models.ForeignKey('LsPromoMaster', db_constraint=False, on_delete=models.DO_NOTHING, null=True)
    user = models.ForeignKey('LsUser', db_constraint=False, on_delete=models.DO_NOTHING)

    pay_method = models.CharField(max_length=45, blank=True, null=True)
    pay_total_price = models.FloatField(blank=True, null=True)
    orig_price = models.FloatField(blank=True, null=True)
    pay_credit = models.FloatField(blank=True, null=True)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_ORDER_GROUP'
        verbose_name = 'Order Group'
        verbose_name_plural = 'Order Group'


class LsOrder(models.Model):
    order_group = models.ForeignKey('LsOrderGroup', db_constraint=False,
                                    on_delete=models.DO_NOTHING,
                                    related_name='orders')
    rest = models.ForeignKey('LsRestaurants',
                             db_constraint=False,
                             on_delete=models.DO_NOTHING,
                             related_name='orders')

    status_code = models.CharField(max_length=45, default='WAIT_PAY')
    # 0.구매취소 : CANCEL_BUY
    # 1.결제대기 : WAIT_PAY
    # 2.결제확인 : COM_PAY
    # 3. 결제오류 : ERR_PAY
    # 4. 결제취소 : CANCEL_PAY
    # 6. 픽업완료 : COM_TAKE
    tip_price = models.FloatField(blank=True, null=True)

    order_no = models.CharField(unique=True, max_length=45, null=True)
    reserve_time = models.DateTimeField(blank=True, null=True)

    orig_price = models.FloatField(blank=True, null=True)
    pay_price = models.FloatField(blank=True, null=True)

    contact_email = models.CharField(max_length=128, blank=True, null=True)
    contact_first_name = models.CharField(max_length=128, blank=True, null=True)
    contact_last_name = models.CharField(max_length=128, blank=True, null=True)
    contact_phone = models.CharField(max_length=256, blank=True, null=True)

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_ORDER'
        verbose_name = 'Order'
        verbose_name_plural = 'Order'


class LsOrderItem(models.Model):
    menu = models.ForeignKey('LsRestaurantsMenu', db_constraint=False, on_delete=models.DO_NOTHING)
    extra = models.ForeignKey('LsRestaurantsMenuExtra', db_constraint=False, on_delete=models.DO_NOTHING, null=True)
    order = models.ForeignKey('LsOrder', db_constraint=False, on_delete=models.DO_NOTHING, related_name='items')

    addons = models.ManyToManyField('LsRestaurantsMenuAddon', through='LsOrderItemAddon')
    instruction = models.TextField(blank=True, null=True)

    quantity = models.IntegerField()
    is_recommended = models.BooleanField(default=False)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_ORDER_ITEM'
        verbose_name = 'Order Item'
        verbose_name_plural = 'Order Item'


class LsOrderItemAddon(models.Model):
    item = models.ForeignKey('LsOrderItem', db_constraint=False, on_delete=models.DO_NOTHING)
    addon = models.ForeignKey('LsRestaurantsMenuAddon', db_constraint=False, on_delete=models.DO_NOTHING)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_ORDER_ITEM_ADDON'
        unique_together = (('item', 'addon'),)
        verbose_name = 'Order Item Addon'
        verbose_name_plural = 'Order Item Addon'


class LsOrderItemAllergy(models.Model):
    item = models.ForeignKey('LsOrderItem', db_constraint=False, on_delete=models.DO_NOTHING, related_name='allergies')
    name = models.CharField(max_length=100)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_ORDER_ITEM_ALLERGY'


def generate_rest_no():
    rest_no = get_random_string(length=5, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    return rest_no


class LsRestaurants(models_gis.Model):
    id = models_gis.AutoField(primary_key=True)
    name = models_gis.CharField(max_length=512)
    desc = models_gis.TextField(blank=True, null=True)

    rest_no = models.CharField(max_length=5, default=generate_rest_no, unique=True)
    service_fee_amount = models_gis.FloatField(blank=True, null=True)
    service_fee_type = models_gis.CharField(max_length=10, blank=True, null=True)
    tax_percent = models_gis.FloatField(blank=False, default=10)

    tel = models_gis.CharField(max_length=100, null=True)
    address = models_gis.CharField(max_length=500, null=True)

    geo_point = PointField()

    categories = models_gis.ManyToManyField('LsRestaurantsCategoryMaster',
                                            through='LsRestaurantsCategory',
                                            related_name='categories')

    owners = models_gis.ManyToManyField('LsUser', through='LsUserRestaurants', related_name='owners')
    # op_hours = models.OneToOneField('LsRestaurantsOperationHour')
    logo_url = models_gis.ImageField(blank=True, null=True, upload_to=image_directory_path)
    is_activate = models_gis.BooleanField(default=True)
    min_order_time = models_gis.IntegerField(blank=True, null=True)
    reserve_time_gap = models_gis.IntegerField(blank=True, null=True)
    reserve_max_count = models_gis.IntegerField(blank=True, null=True)
    insert_dt = models_gis.DateTimeField(auto_now_add=True)
    update_dt = models_gis.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS'
        verbose_name = 'Restaurants'
        verbose_name_plural = 'Restaurants'


class LsRestaurantsHeaderImages(models.Model):
    rest = models.ForeignKey('LsRestaurants', db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='image_urls')
    order = models.IntegerField(null=True)
    url = models.ImageField(upload_to=image_directory_path)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.url.url

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_HEADER_IMAGES'
        verbose_name = 'Restaurants Header Images'
        verbose_name_plural = 'Restaurants Header Images'


class LsUserRestaurants(models.Model):
    user = models.ForeignKey('LsUser', db_constraint=False, on_delete=models.DO_NOTHING)
    rest = models.ForeignKey('LsRestaurants', db_constraint=False, on_delete=models.DO_NOTHING)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_USER_RESTAURANTS'
        unique_together = (('user', 'rest'),)


class LsRestaurantsCategory(models.Model):
    category = models.ForeignKey('LsRestaurantsCategoryMaster', db_constraint=False, on_delete=models.DO_NOTHING)
    rest = models.ForeignKey(LsRestaurants, db_constraint=False, on_delete=models.DO_NOTHING)

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_CATEGORY'
        unique_together = (('category', 'rest'),)
        verbose_name = 'Restaurants Category'
        verbose_name_plural = 'Restaurants Category'


class LsRestaurantsCategoryMaster(models.Model):
    name = models.CharField(max_length=45)
    group_name = models.CharField(max_length=20)

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_CATEGORY_MASTER'
        verbose_name = 'Restaurants Category'
        verbose_name_plural = 'Restaurants Category'


class LsPromoMaster(models.Model):
    desc = models.TextField()
    code = models.CharField(max_length=45)
    amount = models.FloatField()
    type = models.CharField(max_length=45)

    start_dt = models.DateTimeField()
    end_dt = models.DateTimeField()

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_PROMO_MASTER'
        verbose_name = 'Restaurants Promotion'
        verbose_name_plural = 'Restaurants Promotion'


class LsRestaurantsDiscount(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    menu_id = models.IntegerField()
    is_use = models.BooleanField(default=True)
    start_dt = models.DateTimeField(blank=True, null=True)
    end_dt = models.DateTimeField(blank=True, null=True)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_DISCOUNT'


class LsRestaurantsMenu(models.Model):
    name = models.CharField(max_length=128)
    desc = models.CharField(max_length=128, blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)
    rest = models.ForeignKey('LsRestaurants', db_constraint=False, on_delete=models.DO_NOTHING, related_name='menus')
    category = models.ForeignKey('LsRestaurantsMenuCategory', db_constraint=False, on_delete=models.DO_NOTHING)

    image_url = models.ImageField(upload_to=image_directory_path)
    orig_price = models.FloatField()
    display_price = models.FloatField()
    is_hide = models.BooleanField(default=False)
    is_sold_out = models.BooleanField(default=False)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_MENU'
        verbose_name = 'Menu'
        verbose_name_plural = 'Menu'


class LsRestaurantsMenuAddon(models.Model):
    price = models.FloatField()
    name = models.CharField(max_length=128)
    menu = models.ForeignKey('LsRestaurantsMenu',
                             db_constraint=False,
                             on_delete=models.DO_NOTHING,
                             related_name='addons')

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_MENU_ADDON'
        verbose_name = 'Restaurants Menu Addon'
        verbose_name_plural = 'Restaurants Menu Addon'


class LsRestaurantsMenuCategory(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_MENU_CATEGORY'
        verbose_name = 'Menu Category'
        verbose_name_plural = 'Menu Category'


class LsRestaurantsMenuExtra(models.Model):
    name = models.CharField(max_length=128)
    price = models.FloatField()
    menu = models.ForeignKey('LsRestaurantsMenu', db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='extras')
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_MENU_EXTRA'
        verbose_name = 'Restaurants Menu Extra'
        verbose_name_plural = 'Restaurants Menu Extra'


class LsRestaurantsOperationHour(models.Model):
    rest = models.OneToOneField(LsRestaurants, related_name='op_hours', on_delete=models.DO_NOTHING)
    monday_open_hour = models.CharField(max_length=128, default='09:00')
    monday_close_hour = models.CharField(max_length=128, default='18:00')
    tuesday_open_hour = models.CharField(max_length=128, default='09:00')
    tuesday_close_hour = models.CharField(max_length=128, default='18:00')
    wednesday_open_hour = models.CharField(max_length=128, default='09:00')
    wednesday_close_hour = models.CharField(max_length=128, default='18:00')
    thursday_open_hour = models.CharField(max_length=128, default='09:00')
    thursday_close_hour = models.CharField(max_length=128, default='18:00')
    friday_open_hour = models.CharField(max_length=128, default='09:00')
    friday_close_hour = models.CharField(max_length=128, default='18:00')
    saturday_open_hour = models.CharField(max_length=128, default='09:00')
    saturday_close_hour = models.CharField(max_length=128, default='18:00')
    sunday_open_hour = models.CharField(max_length=128)
    sunday_close_hour = models.CharField(max_length=128)
    break_start_hour = models.CharField(max_length=128, null=True, blank=True)
    break_end_hour = models.CharField(max_length=128, null=True, blank=True)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_OPERATION_HOUR'
        verbose_name = 'Operation Hours'
        verbose_name_plural = 'Operation Hours'


class LsRestaurantsHoliday(models.Model):
    rest = models.ForeignKey(LsRestaurants, db_constraint=False, on_delete=models.DO_NOTHING
                                , related_name='holidays')
    holiday = models.DateField(null=False)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_HOLIDAY'
        verbose_name = 'Holidays'
        verbose_name_plural = 'Holidays'


class LsRestaurantsReview(models.Model):
    rest = models.ForeignKey('LsRestaurants', db_constraint=False, related_name='reviews', on_delete=models.DO_NOTHING)
    order = models.OneToOneField('LsOrder', db_constraint=False, related_name='order', on_delete=models.DO_NOTHING, null=True)

    user = models.ForeignKey('LsUser', db_constraint=False, related_name='reviews', on_delete=models.DO_NOTHING)
    tags = models.ManyToManyField('LsRestaurantsReviewTagMaster', through='LsRestaurantsReviewTag')
    rating = models.FloatField()
    contents = models.TextField(blank=True, null=True)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_REVIEW'
        verbose_name = 'Restaurants Reviews'
        verbose_name_plural = 'Restaurants Reviews'


class LsRestaurantsReviewPhoto(models.Model):
    photo_url = models.CharField(max_length=200, blank=True, null=True)
    review = models.ForeignKey('LsRestaurantsReview',
                               db_constraint=False,
                               related_name='photos',
                               on_delete=models.DO_NOTHING)

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.photo_url

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_REVIEW_PHOTO'
        verbose_name = 'Restaurants Review Photo'
        verbose_name_plural = 'Restaurants Review Photo'


class LsRestaurantsReviewTag(models.Model):
    tag = models.ForeignKey('LsRestaurantsReviewTagMaster', db_constraint=False, on_delete=models.DO_NOTHING)
    review = models.ForeignKey('LsRestaurantsReview', db_constraint=False, on_delete=models.DO_NOTHING)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_REVIEW_TAG'
        unique_together = (('tag', 'review'),)


class LsRestaurantsReviewTagMaster(models.Model):
    name = models.CharField(max_length=45, unique=True)
    rating = models.FloatField()
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'LS_RESTAURANTS_REVIEW_TAG_MASTER'
        verbose_name = 'Restaurants Review Tag'
        verbose_name_plural = 'Restaurants Review Tag'


class LsUserCreditLog(models.Model):
    user = models.ForeignKey('LsUser', db_constraint=False, on_delete=models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'LS_USER_CREDIT_LOG'


class LsUserRestaurantsFavorite(models.Model):
    user = models.ForeignKey('LsUser', db_constraint=False, on_delete=models.DO_NOTHING,related_name='favorites')
    rest = models.ForeignKey('LsRestaurants', db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='favorites')

    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_USER_RESTAURANTS_FAVORITE'
        unique_together = (('user', 'rest'),)


class LsAllergyMaster(models.Model):
    name = models.CharField(max_length=100)
    insert_dt = models.DateTimeField(auto_now_add=True)
    update_dt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LS_ALLERGY_MASTER'

