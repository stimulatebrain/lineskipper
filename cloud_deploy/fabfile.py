__author__ = 'jspark'

from fabric.api import *


def production():
                env.hosts = ["skipline4.me"]
                env.user = 'ubuntu'
                env.key_filename = './lineskipper.pem'
                env.deploy_user = "ubuntu"
                env.home_base = "/usr/src/app"
                env.app_base = "/usr/src/app/lineskipper"
                env.django_module = "lineskipper.settings.production"

def devel():
                env.hosts = ["1.255.57.208"]
                env.user = 'root'
                env.password= 'vlrtm@123'
                env.deploy_user = "root"
                env.home_base = "/usr/src/app"
                env.app_base = "/usr/src/app/shoppin_cart"
                env.django_module = "shoppin_cart.settings.local"


def configure_nginx():
                with cd("/etc/nginx"):
                    put("config/nginx.conf", "nginx.conf", use_sudo=True)

                    sudo("chown root:root nginx.conf")
                    sudo("/etc/init.d/nginx restart")


def deploy(branch="master"):
                with cd(env.app_base):
                    if env.django_module == 'shoppin_cart.settings.local':
                        branch = 'develop'
                    sudo('git stash')
                    sudo('git pull origin %s' % branch)


def start(server="gunicorn"):
                if server == "nginx":
                    sudo("/etc/init.d/nginx restart")
                elif server == "gunicorn":
                        with cd(env.app_base):
                            with settings(warn_only=True):
                                    # sudo("/etc/init.d/nginx restart")
                                    # sudo("killall gunicorn")
                                    # sudo("kill `sudo lsof -t -i:8008`")
                                    sudo("kill $(cat gunicorn.pid)")
                                    sudo("DJANGO_SETTINGS_MODULE=" + env.django_module +
                                         " gunicorn --bind=0.0.0.0:8001 --workers=4 "
                                         + "lineskipper.wsgi --pid gunicorn.pid")


def stop(server="gunicorn"):
                if server == "nginx":
                    sudo("/etc/init.d/nginx stop")
                elif server == "gunicorn":
                    with cd(env.app_base):
                        with settings(warn_only=True):
                            sudo("kill $(cat gunicorn.pid)")


def collectstatic():
                with cd(env.app_base):
                    sudo("DJANGO_SETTINGS_MODULE=%s python3 manage.py collectstatic" % env.django_module)
