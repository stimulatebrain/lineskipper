Django == 2.0
Fabric == 1.10.2
mysqlclient == 1.3.12
django-mathfilters==0.3.0
django-redis==4.3.0
django-static-precompiler==1.5
django_debug_toolbar == 1.9.1
django_storages == 1.4.1
requests == 2.5.1
simplejson == 3.5.2
djangorestframework == 3.7.3
django-filter == 1.1.0
markdown == 2.6.9
django-extra-fields==0.9
django-map-widgets==0.1.9
drf-nested-routers==0.90.0
coreapi==2.3.3
celery==4.1.0
django-tabbed-admin==1.0.2
boto3==1.5.6
pillow==4.3.0
